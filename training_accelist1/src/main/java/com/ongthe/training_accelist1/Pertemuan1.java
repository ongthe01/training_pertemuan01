/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ongthe.training_accelist1;

import java.util.*;

/**
 *
 * @author Ongthe
 */

public class Pertemuan1 {
    
    //final static Scanner INPUT = new Scanner(System.in);
    Scanner sc = new Scanner(System.in);
    
    public Pertemuan1(){
        // System.out.println("Hello World");
        
        
        /*System.out.print("Input score: ");
        int score = sc.nextInt();
        System.out.println("Your score: "+score);*/
        
        /*if (score < 80) {
            System.out.println("Grade B");
        } 
        else if(score < 70) {
            System.out.println("Grade C");
        }
        else{
            System.out.println("Grade D");
        }*/
        
        //Switch Case
        /*switch(score){
            case 80:
                 System.out.println("Grade B");
                break;
            case 70:
                System.out.println("Grade C");
                break;
            default:
                System.out.println("Grade D");
        }*/
        
//        for(var i=0; i<10; i++){
//            System.out.print(i+" ");
//        }

        //Do While
        /*int x=0;
        do {            
            x++;
            System.out.print(x+"");
        } while (x<5);*/
        
        //Foreach
       /* String nameList[] = {"Budi","Doni","Tuti"};
        for (var myList : nameList) {
            System.out.print(myList + " ");
        }*/
    }
    
    //No 1
     public void numb1(){
        int numb = 0;
        System.out.print("Input number: ");
        numb = sc.nextInt();
        for (int i = 2; i <= numb; i++) {
            boolean checkPrim = true;
            
            for(int j = 2; j < i ; j++){
                if(j%2 == 1){
                   checkPrim = false; 
                }
                if (i%j == 0) {
                   checkPrim = false;
                }
                break;
            }
            
            if(checkPrim == true){
                System.out.print(i + " ");
            }
        }
    }
    
    //No 2 
    //1 5 14 30 55 91 140
     // 2*2+1
     public void numb2(){
         int numb[]={1, 5, 14, 30, 55, 91, 140};
         int total=0;
         
         for(int i=0; i<7; i++){
             //numb = sc.nextInt();
             for (int j = 1; j <= i; j++) {
                 total = numb[i]*numb[i] +(numb[i]-(i-1));
             }
             System.out.print(total + " ");
         }
     }
     
    //No 3
     //8 27 125 512 1728 = 2 3 5 8 12
     public void numb3(){
         int numb[]={2, 3, 5, 8, 12};
         
         for (int i = 0; i <5; i++) {
              //numb = sc.nextInt();
              
              int power = numb[i]*numb[i]*numb[i];
              System.out.print(power + " ");
         }
     }
     //Pakai input
     /*public void numb3(){
         int numb=0;
         
         for (int i = 0; i < 5; i++) {
              numb = sc.nextInt();
              int power = numb*numb*numb;
              System.out.print(power + " ");
         }
     }*/
     
    //No 4
     public void numb4(){
         int duck=0, cage=0;
         System.out.print("Duck: ");
         duck = sc.nextInt();
         System.out.print("Cage: ");
         cage = sc.nextInt();
         
         if(duck%cage == 1){
             int total1 = duck/cage + 1;
             System.out.println("Cages: " + total1);
         }
         else if(duck%cage == 0){
             int total2 = duck/cage;
             System.out.println("Cages: " + total2);
         }
     }
     
    //No 5
    public void numb5(){
        int input=0;
        
        System.out.print("Input number: ");
        input = sc.nextInt();
        
            if(input%2==0){
                System.out.print("a");
            }
            if(input%3==0){
                System.out.print("b");
            }
            if(input%5==0){
                System.out.print("c");
            }
            if(input%7==0){
                System.out.print("d");
            }
            if(input%10==0){
                System.out.print("e");
            }
            if(input%15==0){
                System.out.print("f");
            } 
    } 
    
    public void numb6(){
        //angka ganjil antara 10-15
        ArrayList ganjil = new ArrayList();
        ArrayList genap = new ArrayList();
        
        int start=0, end=0;
        //int genap=0, ganjil=0;
        
        System.out.print("Start: ");
        start = sc.nextInt(); sc.nextLine();
        System.out.print("End: ");
        end = sc.nextInt();
        
        for(int i = start+1 ; i<end; i++){   
                if (i%2==0) {
                    genap.add(i);
                }
                 if (i%2==1) {
                    ganjil.add(i);
                }
        }

        System.out.println("Bilangan genap antara " + start
                    + " dan " + end + " adalah ");
        for (Object obj1 : genap) {
            System.out.print(obj1+" ");
        }
        
        System.out.println("\nBilangan ganjil antara " + start
                    + " dan " + end + " adalah ");
        for (Object obj2 : ganjil) {
             System.out.print(obj2+" ");
        }
    }
    
    public static void main(String[] args){
       Pertemuan1 pt = new Pertemuan1();
       //pt.numb1();
       //pt.numb2();
       pt.numb3();
       //pt.numb4();
       //pt.numb5();
       //pt.numb6();
    }
}